package com.practica1.practica1.models;

import java.util.HashMap;
import java.util.Map;

public class PurchaseModel {

    private String id;
    private String userId;
    private float amount;
    Map<String,Integer> purchaseItems = new HashMap<>();


    public PurchaseModel() {
    }

    public PurchaseModel(String id, String userId, float amount, Map<String, Integer> purchaseItems) {
        this.id = id;
        this.userId = userId;
        this.amount = amount;
        this.purchaseItems = purchaseItems;
    }

    public String getId() {
        return this.id;
    }

    public String getUserId() {
        return this.userId;
    }

    public float getAmount() {
        return this.amount;
    }

    public Map<String, Integer> getPurchaseItems() {
        return this.purchaseItems;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public void setPurchaseItems(Map<String, Integer> purchaseItems) {
        this.purchaseItems = purchaseItems;
    }

}
