package com.practica1.practica1.service;


import com.practica1.practica1.models.UserModel;
import com.practica1.practica1.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    @Autowired
    UserRepository userRepository;

    public List<UserModel> findAll(int age) {
        System.out.println("findAll en UserService " + age);

        return this.userRepository.findAll(age);
    }

    public Optional<UserModel> findById(String id){
        System.out.println("findById in UserService");
        return this.userRepository.findById(id);
    }

    public UserModel adduser(UserModel user) {
        System.out.println("adduser en UserService");
        return this.userRepository.newuser(user);
    }

    public UserModel update (UserModel userModel){
        System.out.println("update en UserService " + userModel.getId());
        return this.userRepository.update(userModel);
    }

    public boolean delete (String id) {
        System.out.println("delete en UserService");
        boolean result = false;
        Optional<UserModel> userToDelete = this.findById(id);
        if (userToDelete.isPresent() == true) {
            result = true;
            this.userRepository.delete(userToDelete.get());
        }
        return result;
    }
}
