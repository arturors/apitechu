package com.practica1.practica1.service;

import com.practica1.practica1.models.PurchaseModel;

import com.practica1.practica1.repositories.PurchaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PurchaseService {


    @Autowired
    PurchaseRepository purchaseRepository;


    public int addpurchase(PurchaseModel purchase) {
        System.out.println("addpurchase en PurcahseService");
        int result = this.purchaseRepository.newpurchase(purchase);
        return result;
    }

    public List<PurchaseModel> findAll() {
        System.out.println("findAll en PurchaseService " );
        return this.purchaseRepository.findAll();
    }
}
