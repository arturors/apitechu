package com.practica1.practica1.controllers;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @RequestMapping("/")
    public String index(){
        return "Hola mundo desde Practica 1";
    }

    @RequestMapping("/hello")
    public String hello(@RequestParam(value ="name", defaultValue = "Practica 1") String name){
        return String.format("Hola %s", name);
    }

}
