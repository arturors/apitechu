package com.practica1.practica1.controllers;


import com.practica1.practica1.models.UserModel;
import com.practica1.practica1.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/practica1/v1")
public class UserController {


    @Autowired
    UserService userService;


    @GetMapping("/users")
    public ResponseEntity<List<UserModel>> getUsers(@RequestParam(value="age",required = false,
            defaultValue = "0") int age) {

        System.out.println("getUsers");
        System.out.println("La edad en queryString es " + age);
        List<UserModel> result = this.userService.findAll(age);
        return new ResponseEntity<>(
                result,HttpStatus.OK
        );
    }


    @GetMapping("/users/{id}")
    public ResponseEntity<Object> getUserById(@PathVariable String id){
        System.out.println("getUserById");
        System.out.println("La id del usuario a buscar es "+id);

        Optional<UserModel> result = this.userService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Usuario no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );

    }

    @PostMapping("/users")
    public ResponseEntity<UserModel> adduser(@RequestBody UserModel user) {
        System.out.println("addaser");
        System.out.println("La id del usuario nuevo es " + user.getId());
        System.out.println("El nombre del usario nuevo es " + user.getName());
        System.out.println("La edad del usuari nuevo es " + user.getAge());
        return new ResponseEntity<>(
                this.userService.adduser(user),
                HttpStatus.CREATED
        );
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<UserModel> updateUser(@RequestBody UserModel user, @PathVariable String id) {
        System.out.println("updateUser");
        System.out.println("La id del usuario a actualizar en parametro es " + id);
        System.out.println("El nombre a actualizar es " + user.getName());
        System.out.println("La edad a acutalizar es " + user.getAge());
        user.setId(id);
        return new ResponseEntity<>(this.userService.update(user), HttpStatus.OK);
    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity<String> deleteuser(@PathVariable String id){
        System.out.println("deleteUser");
        System.out.println("La ide del usuario a borrar es " + id);
        boolean deleteUser = this.userService.delete(id);
        return new ResponseEntity<>(
                deleteUser ? "Usuario borrado" : "Usuario no encontrado",
                deleteUser ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

}
