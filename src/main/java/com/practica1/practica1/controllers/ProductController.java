package com.practica1.practica1.controllers;


import com.practica1.practica1.models.ProductModel;
import com.practica1.practica1.service.ProductService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/practica1/v1")

public class ProductController {

    @Autowired
    ProductService productService;

    @GetMapping("/products")
    public ResponseEntity<List<ProductModel>> getProducts() {
        System.out.println("getProducts");

        return new ResponseEntity<>(
                this.productService.findAll(),
                HttpStatus.OK
        );
    }

    @PostMapping("/products")
    public ResponseEntity<ProductModel> addProduct(@RequestBody ProductModel product) {
        System.out.println("addProduct");
        System.out.println("La id del producto a crear es " + product.getId());
        System.out.println("La descripción del producto a crear es " + product.getDesc());
        System.out.println("El precio del producto a crear es " + product.getPrice());

        return new ResponseEntity<>(
                this.productService.add(product),
                HttpStatus.CREATED
        );
    }
    @GetMapping("/products/{id}")
    public ResponseEntity<Object> getProductById(@PathVariable String id){
        System.out.println("getProductById");
        System.out.println("La id del producto a buscar es "+id);

        Optional<ProductModel> result = this.productService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Producto no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );

    }

    @PutMapping("/products/{id}")
    public ResponseEntity<ProductModel> updataProduct(@RequestBody ProductModel product, @PathVariable String id) {
        System.out.println("updateProduct");
        System.out.println("La id del producto a actualizar en parametro es " + id);
        System.out.println("La id a actualizar es " + product.getId());
        System.out.println("La descripción a actualizar es " + product.getDesc());
        System.out.println("el precio a actualizar es " + product.getPrice());

        return new ResponseEntity<>(this.productService.update(product), HttpStatus.OK);
    }

    @DeleteMapping("products/{id}")
    public ResponseEntity<String> deleteProduct(@PathVariable String id) {
        System.out.println("deleteProduct");
        System.out.println("La id del producto a borrar es " + id);
        boolean deleteProduct = this.productService.delete(id);
        return new ResponseEntity<>(
                deleteProduct ? "Producto borrado" : "Producto no encontrado",
                deleteProduct ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }
}


