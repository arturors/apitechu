package com.practica1.practica1;

import com.practica1.practica1.models.PurchaseModel;
import com.practica1.practica1.models.UserModel;
import com.practica1.practica1.models.ProductModel;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;

@SpringBootApplication
public class Practica1Application {

	public static ArrayList<UserModel> userModels;
	public static ArrayList<ProductModel> productModels;
	public static ArrayList<PurchaseModel> purchaseModels;

	public static void main(String[] args) {
		SpringApplication.run(Practica1Application.class, args);

		Practica1Application.userModels = Practica1Application.getTestData();
		Practica1Application.productModels = Practica1Application.getTestData();
		Practica1Application.purchaseModels = Practica1Application.getPurchaseTestData();
	}


	private static ArrayList<ProductModel> getTestData() {

		ArrayList<ProductModel> productModels = new ArrayList<>();

		productModels.add(
				new ProductModel(
						"1"
						,"Producto 1"
						,10
				)
		);

		productModels.add(
				new ProductModel(
						"2"
						,"Producto 2"
						,20
				)
		);

		productModels.add(
				new ProductModel(
						"3"
						,"Producto 3"
						,30
				)
		);

		return productModels;
	}

	private static ArrayList<UserModel> getTestDato() {

		ArrayList<UserModel> userModels = new ArrayList<>();

		userModels.add(
				new UserModel(
						"1"
						,"Usuario 1"
						,30
				)
		);

		userModels.add(
				new UserModel(
						"2"
						,"Usuario 2"
						,50
				)
		);

		userModels.add(
				new UserModel(
						"3"
						,"Usuario 3"
						,28
				)
		);


		return userModels;
	}

	private static ArrayList<PurchaseModel> getPurchaseTestData() {

		ArrayList<PurchaseModel> purchaseModels = new ArrayList<>();

		return purchaseModels;
	}
}
