package com.practica1.practica1.repositories;


import com.practica1.practica1.Practica1Application;
import com.practica1.practica1.models.PurchaseModel;
import com.practica1.practica1.models.UserModel;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Repository
public class PurchaseRepository {


    @Autowired
    UserRepository userRepository;

    @Autowired
    ProductRepository productRepository;


    public Optional<PurchaseModel> findById(String id) {
        System.out.println("FindById en PurchaseRepository " + id);
        Optional<PurchaseModel> result = Optional.empty();
        for (PurchaseModel purhaseInList : Practica1Application.purchaseModels){
            if (purhaseInList.getId().equals(id)) {
                System.out.println("Compra encontrada en findById de purchase repository");
                result = Optional.of(purhaseInList);
            }
        }
        return result;
    }

    public int newpurchase (PurchaseModel purchase) {
        System.out.println("newpurchase en PurchaseRepository");

        System.out.println("-----validar si existe usuario " + purchase.getUserId());
        Optional<UserModel> usertofind = userRepository.findById(purchase.getUserId());
        System.out.println("-----validar si existe compra " + purchase.getId());
        Optional<PurchaseModel> purchasetofind = this.findById(purchase.getId());
        System.out.println("-----validar si existen productos " + purchase.getPurchaseItems());

        float amount = 0;
        for (Map.Entry<String, Integer> purchaseitem : purchase.getPurchaseItems().entrySet()) {
            if (this.productRepository.findById(purchaseitem.getKey()).isPresent() == true ) {
                System.out.println("producto encontrado");
                System.out.println("Añadiendo valor de "+ purchaseitem.getValue() + "unidades del prdoucto al total ") ;

                amount +=
                        (this.productRepository.findById(purchaseitem.getKey()).get().getPrice()
                                * purchaseitem.getValue());

            } else {
                int result;
                result = 3;
                return result;
            }
        }
        purchase.setAmount(amount);

        int result;
        result = 0;

        if (usertofind.isPresent() == true ) {
            result = 0;
        } else {
            result = 1;
        }

        System.out.println("**purchasetofind.ispresent " + purchasetofind.isPresent());
        if (purchasetofind.isPresent() == true) {
            result = 2;
        } else {
            result = 0;
        }


        if (result == 0){
            System.out.println("En PurchaseRepository añadiendo compra");
            Practica1Application.purchaseModels.add(purchase);
        }
        return result;
    }


    public List<PurchaseModel> findAll() {
        System.out.println("findAll en PurchaseRepository");
        return Practica1Application.purchaseModels;
    }

}
